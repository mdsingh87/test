<?php
include_once 'books.php';
if(!isset($_SESSION['user_id']) || empty($_SESSION['user_id'])) {
    header("Location: ".BASE_URL."login.php");
    exit();
}
//get user id and book path param
$user_id = isset($_GET['user_id']) ? trim($_GET['user_id']) : '';
$book    = isset($_GET['book']) ? trim($_GET['book']) : '';
if($user_id != $_SESSION['user_id']) {
    header("Location: ".BASE_URL."denied.php");
    exit();
}
if(!empty($user_id)) {
    //$_SESSION['user_id'] = $user_id;
    $books  = new Books($user_id);
    $lists   = $books->get_list();
    //echo "<pre>";print_r($list);die;
    ?>
    
<!DOCTYPE html>
<html>

    <head>
        <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    </head>
    <body>
        <style>
            body {
                background:#F0F0F0;
            }

            .form_bg {
                background-color:#fff;
                color:#666;
                padding:20px;
                border-radius:10px;
                position: absolute;
                border:1px solid #f3f3f3;
                margin: auto;
                top: 0;
                right: 0;
                bottom: 0;
                left: 0;
                width: 80%;
                height: 90%
            }

            .align-center {

                text-align:center;
            }
            table td {
                padding: 5px;
                width: 20%;
            }
            img {
                width: 100%;
                border: 1px solid #f0f0f0;
            }
        </style>
        <div class="container">
            <div class="row">
                <div class="form_bg">
                    <p style="padding: 20px;float:right;"><a href="<?php echo BASE_URL. 'logout.php'; ?>">Logout <?php echo $_SESSION['username']; ?></a></p>
                    <br><br>
                    <h3>Books Listing</h3>
                    <table>
                        <tbody>
                            <tr>
                            <?php
                            if(!empty($lists) && is_array($lists)) {
                                foreach($lists as $list) {
                                    $path = '';
                                    if($list['path'] != 'tiger-2018') {
                                        $path = $list['path'].'/';
                                    }
                                    echo '<td><a href="'.BASE_URL.'proxy.php?book='.$list['path'].'" title="'.$list['title'].'"><img src="'.BASE_URL.'proxy.php?front=1&image='.$path.'pics/en/page_1.jpg"></a></td>';
                                    //echo '<tr><td><a href="'.BASE_URL.'proxy.php?book='.$list['path'].'" title="'.$list['title'].'">'.$list['title'].'</a></td></tr>';
                                }
                            } else {
                                echo '<p>No records found</p>';
                            }
                            ?>
                            </tr>
                        </tbody>
                    </table>
                    
                </div>
            </div>
        </div>
    </body>
</html>


<?php } else { ?>
<p>We are unable to verify the user.</p>
<?php }
