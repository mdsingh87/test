<?php
include_once 'config.php';
//destroy session
unset($_SESSION['user_id'], $_SESSION['username'], $_SESSION['book']);
header("Location: ".BASE_URL."login.php");
exit();