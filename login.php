<?php 
    include_once 'config.php';
    include('db.php');
    $db = new db();
    $error = '';
    if(isset($_SESSION['user_id']) && !empty($_SESSION['user_id'])) {
        header("Location: ".BASE_URL."book_list.php?user_id=".$_SESSION['user_id']);
        exit();
    }
    $submitted = $db->postVar('submitted');
    if($submitted == 1) {
        //check username and password and provide access
        $username = $db->postVar('username');
        $password = $db->postVar('password');
        if(!empty($username) && !empty($password)) {
            //query database for valid user
            $query = 'SELECT * FROM users WHERE username="'.$username.'" AND password="'.md5($password).'"';
            //echo $query;die;
            $result = $db->getOne($query);
            if(!empty($result)) {
                //grant access and redirects to book list page
                //save to session
                $_SESSION['user_id']  = $result['id'];
                $_SESSION['username'] = $result['username'];
                header("Location: ".BASE_URL."book_list.php?user_id=".$result['id']);
                exit();
            } else {
                //invalid credentials
                $error = 'Invalid Username/Password.';
            }
        } else {
            $error = 'Username and Password are required.';
        }
    }
?>
<!DOCTYPE html>
<html>

    <head>
        <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
       
    </head>
    <body>
        <style>
            body {
                background:#F0F0F0;
            }

            .form_bg {
                background-color:#fff;
                color:#666;
                padding:20px;
                border-radius:10px;
                position: absolute;
                border:1px solid #fff;
                margin: auto;
                top: 0;
                right: 0;
                bottom: 0;
                left: 0;
                width: 390px;
                height: 420px;
            }

            .align-center {

                text-align:center;
            }
            
        </style>
        <div class="container">
            <div class="row">
                <div class="form_bg">
                    <form method="post">
                         <h2 class="text-center">Login Page</h2>
                         <?php if(!empty($error)) { ?>
                         <p style="color:red;"><?php echo $error; ?></p>
                         <?php } ?>
                        <br/>
                        <div class="form-group">
                            <input type="text" class="form-control" name="username" id="username" placeholder="Username" value="<?php echo isset($_POST['username']) ? $_POST['username'] : ''; ?>" required maxlength="10">
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control" name="password" id="password" placeholder="Password" required maxlength="10">

                            </div>
                            <br/>
                           <div class="align-center">
                               <input type="hidden" name="submitted" value="1">
                        <button type="submit" class="btn btn-default" id="login">Login</button>
                            </div>
                    </form>
                    <div><br><i>Demo Credentials : <br>Username: demo1, &nbsp;&nbsp;Password: demo123<br>Username: demo2, &nbsp;&nbsp;Password: demo231<br>Username: demo3, &nbsp;&nbsp;Password: demo321<br></i></div>
                </div>
            </div>
        </div>
    </body>
</html>
