<?php
include_once 'config.php';
// Include the SDK using the Composer autoloader
require 'aws/aws-autoloader.php';

use Aws\Exception\AwsException;
use Aws\S3\S3Client;
use Aws\CommandPool;
use Aws\CommandInterface;
use Aws\ResultInterface;
use GuzzleHttp\Promise\PromiseInterface;


include_once 'books.php';

$db = new db();
if(!isset($_SESSION['user_id']) || empty($_SESSION['user_id'])) {
    header("Location: ".BASE_URL."login.php");
    exit();
}


$s3Url = 'https://s3.amazonaws.com/';
$filename = ($db->getVar('image') != false) ? trim($db->getVar('image')) : '';

if(!isset($_SESSION['book']) || empty($_SESSION['book'])) {
    $book = '';
} else {
    $book = $_SESSION['book'];
}
if(!empty($filename)) {
    //check if book name already included in url
    if(!empty($book)) {
        //extract book name from url
        $explode_url = array_filter(explode('/', $filename));
        if(isset($explode_url[0]) && (trim($explode_url[0], '/') == $book)) {
            $filename = str_replace($book.'/', '', $filename);
        }
    }

    //initialize library connection
    $client = new Aws\S3\S3Client([
        'version'     => 'latest',
        'region'      => 'us-east-1',
        'debug'       => TRUE, // enable debug info
        'stats'       => TRUE, // enable stats
        'http' => array(
                'proxy' => 'http://127.0.0.1:80'
        ),
        'credentials' => [
            'key'    => base64_decode(KEY),
            'secret' => base64_decode(SECRET)
        ]
    ]);
    $result = $client->listBuckets();
// Inspect the stats.
$stats = $result['@metadata']['transferStats'];
// display all information about last transfer

    $bucket = '8janbucket';
    $key    = $filename;
    if(($db->getVar('front') != false) && $db->getVar('front') == 1) 
        $key    = $filename;
    elseif(!empty($book))
        $key    = $book.'/'.$filename;
    
    $files = $client->listObjects(['Bucket' => $bucket, 'Delimiter' => '/']);
   
    //echo "<pre>";print_r($files['CommonPrefixes']);die;
            
            
    $cmd = $client->getCommand('GetObject', [
        'Bucket' => $bucket,
        'Key'    => $key
    ]);

    $request = $client->createPresignedRequest($cmd, '+10 minutes');

    // Get the actual presigned-url
    $presignedUrl = (string) $request->getUri();

    $ext = pathinfo($filename, PATHINFO_EXTENSION);
    
    try {
        setHeaders($presignedUrl);
        
        switch ($ext) {
            case "jpg":
                //header('Content-Type: image/jpeg');
                @readfile($presignedUrl);
                break;
            case "gif":
                //header('Content-Type: image/gif');
                @readfile($presignedUrl);
                break;
            case "png":
               // header('Content-Type: image/png');
                @readfile($presignedUrl);
                break;
            case "css":
               // header('Content-Type: text/css');
                @readfile($presignedUrl);
                break;
                /*$css = file_get_contents($presignedUrl);
                $css .= makeArrayOfBackroundImages($css, BASE_URL.'proxy.php?image='.$book.'/');*
                //echo "<pre>";print_r($css);die;
                header('Content-Type: text/css');
                echo $css;
                break;*/
            case "js":
               // header('Content-Type: text/javascript');
                readfile($presignedUrl);
                break;
            default:
               // header('Content-Type: text/opentype');
                @readfile($presignedUrl);
                break;
            }
    } catch(Exception $e) {
        echo $e->message();
    }

} elseif(!empty($db->getVar('book'))) {
    //check if user have access to this book
    $book = ($db->getVar('book') != false) ? trim($db->getVar('book')) : '';
    if(!empty($book)) {
        $user_id = isset($_SESSION['user_id'])? $_SESSION['user_id'] : '';
        $books  = new Books($user_id, $book);
        $check   = $books->check_auth();
        if(empty($check)) {
            header("Location:".BASE_URL."denied.php");
            exit();
        }
    }
    if(($db->getVar('front') != false) && $db->getVar('front') == 1) {
        //do nothing
    } else {
        $_SESSION['book'] = trim($db->getVar('book'));
    }
    header("location: ".BASE_URL.$_SESSION['book']);
    exit();
}

function setHeaders($presignedUrl) {
    $info = getimagesize($presignedUrl);
    
    /*header("Pragma: private");
    header('Cache-control: max-age='.(60*60*24*7));
    header('Expires: '.gmdate(DATE_RFC1123,time()+60*60*24*7));
    header("Cache-Control: private");*/
    header("Content-Type: {$info['mime']}\n");
    //header("Content-Length: $fs\n");
}

function makeArrayOfBackroundImages($css, $url) {
    $new_css = "\n";
    //if (!preg_match_all('/\.([a-z\d_]+)\s*\{[^{}]*url\("?([^()]+)"?\)?/i', $css, $arr)) return array();
    if (!preg_match_all('/\.([a-z\d_]+)\s*\{[^{}]*url\("?([^()]+)"?\)?/i', $css, $arr)) $new_css .= '';
    //if (!preg_match_all('/\.*url\("?([^()]+)"?\)?/i', $css, $arr)) return array();
    //echo "<pre>";print_r($arr);
    //replace and generate new css to append
    $new_css .= replace_css($arr, $url);
    
    if (!preg_match_all('/\#([a-z\d_]+)\s*\{[^{}]*url\("?([^()]+)"?\)?/i', $css, $arr)) $new_css .= '';
    $new_css .= replace_css($arr, $url);
    
    //return array_combine($arr[1], $arr[2]);
    return $new_css;
}

function replace_css($arr, $url) {
    $new_css = '';
    if(!empty($arr[2])) {
        foreach($arr[2] as $k=>$val) {
            $string = trim(trim($val,'"'), "'");
            if(strpos($string, "./img")) {
                $string = str_replace("../img/", $url.'digital_bookshelf/engine/img/', trim($string));
                $new_css .= ".".$arr[1][$k]." { background-image: url('".$string."') !important;}";
            } elseif(strpos($string, 'img/')) {
                $string = str_replace("img/", $url.'digital_bookshelf/engine/img/', trim($string));
                $new_css .= ".".$arr[1][$k]." { background-image: url('".$string."') !important;}";
            } else {
                $new_css .= ".".$arr[1][$k]." { background-image: url('".$url.'digital_bookshelf/engine/css/'.$val. "'} !important;}";
            }
        }
    }
    return $new_css;
}
