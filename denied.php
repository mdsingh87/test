<?php
   include_once 'config.php';
   if(!isset($_SESSION['user_id']) || empty($_SESSION['user_id'])) {
        header("Location: ".BASE_URL."login.php");
        exit();
    }
?>
<h1>Access Denied!</h1>
<p><a href="<?php echo BASE_URL; ?>book_list.php?user_id=<?php echo $_SESSION['user_id']; ?>">Book List</a></p>