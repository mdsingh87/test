<?php
include('db.php');
class Books {
    public $user_id;
    public $book_path;
    public $db;
    
    public function __construct($user_id, $book_path = '') {
        $this->user_id = $user_id;
        $this->book_path = $book_path;
        $this->db = new db();
    }
    //check if user have access to this book
    public function check_auth() {
        $query = 'SELECT * FROM books b JOIN books_access ba ON (ba.book_id=b.id AND ba.user_id="'.$this->user_id.'") WHERE b.path="'.$this->book_path.'"';
        //echo $query;die;
        $check = $this->db->getOne($query);
        if(!empty($check)) {
            return true;
        }
        return false;
    }
    
    public function get_list() {
        //check if user have access to this book
        $query = 'SELECT * FROM books b JOIN books_access ba ON (ba.book_id=b.id AND ba.user_id="'.$this->user_id.'") GROUP BY book_id Order By b.title ASC';
        //echo $query;die;
        $results = $this->db->getAll($query);
        if(!empty($results)) {
            return $results;
        }
        return false;
    }
}